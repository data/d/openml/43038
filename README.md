# OpenML dataset: dow-jones-index

https://www.openml.org/d/43038

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Dr. Michael Brown
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/dow+jones+index) - 2017
**Please cite**: [Paper](https://link.springer.com/content/pdf/10.1007%2F978-3-642-39712-7_3.pdf)  

**Dow Jones Index Data Set**

In our research each record (row) is data for a week.  Each record also has the percentage of return that stock has in the following week (percent_change_next_weeks_price). Ideally, you want to determine which stock will produce the greatest rate of return in the following week.  This can help you train and test your algorithm.

Some of these attributes might not be use used in your research.  They were
originally added to our database to perform calculations.  (Brown, Pelosi & Dirska, 2013) used percent_change_price, percent_change_volume_over_last_wk, days_to_next_dividend,  and percent_return_next_dividend.  We left the other attributes in the dataset in case you wanted to use any of them. Of course what you want to maximize is percent_change_next_weeks_price.

**Training data vs Test data**:
In (Brown, Pelosi & Dirska, 2013) we used quarter 1 (Jan-Mar) data for training and quarter 2 (Apr-Jun) data for testing.

**Interesting data points**:
If you use quarter 2 data for testing, you will notice something interesting in 
the week ending 5/27/2011 every Dow Jones Index stock lost money.


### Attribute information

- quarter:  the yearly quarter (1 = Jan-Mar; 2 = Apr=Jun)
- stock: the stock symbol (see above)
- date: the last business day of the work (this is typically a Friday)
- open: the price of the stock at the beginning of the week
- high: the highest price of the stock during the week
- low: the lowest price of the stock during the week
- close: the price of the stock at the end of the week
- volume: the number of shares of stock that traded hands in the week
- percent_change_price: the percentage change in price throughout the week
- percent_chagne_volume_over_last_wek: the percentage change in the number of shares of  stock that traded hands for this week compared to the previous week
- previous_weeks_volume: the number of shares of stock that traded hands in the previous week
- next_weeks_open: the opening price of the stock in the following week
- next_weeks_close: the closing price of the stock in the following week
- percent_change_next_weeks_price: the percentage change in price of the stock in the 
- following week days_to_next_dividend: the number of days until the next dividend
- percent_return_next_dividend: the percentage of return on the next dividend

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43038) of an [OpenML dataset](https://www.openml.org/d/43038). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43038/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43038/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43038/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

